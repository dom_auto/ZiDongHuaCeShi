#!/usr/bin/evn python
# -*- coding:utf-8 -*-
import time
import unittest
import os
from shadon.HTMLTestRunner import HTMLTestRunner


def create_html():
    # 检查是否有firefox进程，进行关闭
    ret = 1
    current_number = 0
    while current_number < 3:
        if ret == 0:
            print("目标进程存在，杀死该进程")
            os.system('TASKKILL /F /IM firefox.exe"')
            ret = 1
            current_number += 1
            continue
        else:
            print("目标进程不存在")
            print(time.time())
            ret = os.system('tasklist | find "firefox.exe"')
            print(ret)
            print('-' * 50)
            current_number += 1
        time.sleep(1)
    # 生成测试报告
    global filename,now
    filename = 'null'
    # 用例路径
    case_path = os.path.join(os.getcwd(), "src")
    # 报告路径
    report_path = os.path.join(os.getcwd(), "report")
    # 运行case路径下所有以test结尾的案例
    discover = unittest.defaultTestLoader.discover(case_path, pattern="*test.py", top_level_dir=None)
    now = time.strftime("%Y-%m-%d %H_%M_%S")
    # 报告名称
    filename = report_path + '/' + now + '_result.html'
    # print(filename)
    fp = open(filename, 'wb')
    runner = HTMLTestRunner(stream=fp, title='衣联网系统UI测试报告:', description='测试用例如下:')
    runner.run(discover)
    fp.close()