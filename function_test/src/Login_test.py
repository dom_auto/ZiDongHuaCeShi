# coding=utf-8

import unittest,time,os
from selenium import webdriver
from shadon.log import logger
from shadon.global_control import Global_control
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By

class Login(unittest.TestCase):
    '''衣联网登录'''
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.base_url = "https://www.eelly.com/"
        self.driver.implicitly_wait(30)
        self.judge = ''   #用来判断脚本是否执行到断言，没有执行则直接把测试结果置为False,然后系统会给相关人员发送邮件
        self.Ins = Global_control()  #实例化导入的类，模块中的方法才能调用该类中的方法
    def login(self):
        '''衣联网登录'''
        logger.info('开始调用login方法')
        driver = self.driver
        driver.get(self.base_url)
        driver.maximize_window()
     #  time.sleep(3) #等待10秒
        driver.find_element_by_xpath(".//*[@id='js_login_info']/div[2]/a[1]").click()
      #  time.sleep(2)
        driver.find_element_by_name("account_login").send_keys("13827757741")
        driver.find_element_by_name("password").send_keys("1Q2W3e4r.")
        driver.find_element_by_name("submit_login").click()
        WebDriverWait(driver,30,1).until(EC.visibility_of_element_located((By.XPATH,".//*[@id='sitenav']/div/div[2]/a")))
        try:
            self.judge = True
            self.assertEqual(u"衣联网,服装批发市场新的领航者,广州十三行,杭州四季青2016新款品牌男装女装批发", self.driver.title)
        except AssertionError:
            logger.info("断言失败")
            Global_control.Run_result = False
            self.Ins.screen_shot()   #进行判断，看截图文件夹是否创建，创建则跳过，否则创建文件夹
            driver.get_screenshot_as_file(Global_control.Screen_path + "/" + "衣联网登录断言失败"+ ".jpg")
            raise "测试出现错误，需要发送邮件"
    def tearDown(self):
        '''关闭浏览器'''
        if self.judge != True:
            Global_control.Run_result = False   #增加一步判断，避免出现脚本未执行到断言，而系统没有抛出异常
            self.Ins.screen_shot()  # 进行判断，看截图文件夹是否创建，创建则跳过，否则创建文件夹
            self.driver.get_screenshot_as_file(Global_control.Screen_path + "/" + "衣联网登录失败"+ ".jpg")
        self.driver.quit()
    def test_demo(self):
        # 整个接口需要调用的方法，都通过该方法进行调用，按顺序调用方法
        '''login》登录衣联网成功'''
        Login.login(self)

if __name__ == "__main__":
   suite = unittest.TestLoader().loadTestsFromTestCase(Login.test_demo)
   unittest.TextTestRunner(verbosity=2).run(suite)