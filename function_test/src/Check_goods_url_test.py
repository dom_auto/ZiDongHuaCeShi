#!/usr/bin/evn python
# -*- coding:utf-8 -*-

# Author: xietian
# Created Time: 20171223
import unittest
import requests
from shadon.log import logger
from shadon.global_control import Global_control

class Check_goods_url(unittest.TestCase):
    '''检查线上服务器是否能正常打开系统地址'''
    def setUp(self):
         self.url ='https://m.eelly.com/goods/6550142.html'
         self.judge = ''   #用来判断脚本是否执行到断言，没有执行则直接把测试结果置为False,然后系统会给相关人员发送邮件
    def Check_goods_url(self):
        '''检查线上服务器是否能正常打开系统地址'''
        logger.info('开始调用Check_goods_url方法')
        r = requests.post(self.url)              #发送请求
        result = r.headers                       #获取结果
        logger.info(result)  # 打印结果到日志
        try:
            self.assertEqual(r.status_code, 200)  # 断言结果
            self.assertIsNotNone(result['Date'])
            self.assertEqual(result['Content-Type'], "text/html; charset=utf-8")
            self.assertEqual(result['Connection'], 'keep-alive')
            self.judge = True
        except:
            Global_control.Run_result = False
            logger.info("断言异常")
            raise "测试出现错误，需要发送邮件"
    def tearDown(self):
        if self.judge != True:
            Global_control.Run_result = False   #增加一步判断，避免出现脚本未执行到断言，而系统没有抛出异常
    def test_demo(self):
        #整个接口需要调用的方法，都通过该方法进行调用，按顺序调用方法
        '''Check_goods_url》检查线上服务器是否能正常打开系统地址'''
        Check_goods_url.Check_goods_url(self)


if __name__ == "__main__":
   suite = unittest.TestLoader().loadTestsFromTestCase(Check_goods_url.test_demo)
   unittest.TextTestRunner(verbosity=2).run(suite)